# 9. 그리디 알고리즘 (Greedy Algorithms)

### 1. 그리디 알고리즘이란?

- 최적화 문제를 해결하기 위한 알고리즘 중 하나

- 항상 각 단계에 있어서 가장 좋을 것이라는 답을 선택함

- 동적 프로그래밍에서 최적의 해를 찾으려면 너무 많은 연산을 해야 하는 경우가 발생

- 항상 최적의 답을 구하는 것은 아님

- 최소 신장 트리 (Minimum Spanning Tree)가 그리디 알고리즘으로 최적의 해를 찾는 예중 하나

- 그리디 알고리즘을 활용한 잘못된 예 : 이진 트리에서 최대합의 경로 찾기

![binary](./img/binary.png)  



### 2. 그리디 알고리즘을 적용하는 활동 선택 문제 

- 활동의 시작 시간과 종료 시간이 아래의 표와 같다고 할때 (ex. 회의실 배정 문제)

![activity](./img/activity.png)

최대 활동이 선택 될 수 있도록 하려면 가장 빨리 끝나는 활동을 선택해야 한다. 그래야 그 이후에 사용할 수 있는 활동의 시간이 최대가 되기 때문이다.

![activity2](./img/activity2.png)

1. 동적 프로그래밍 방식

하나의 활동을 선택하는 경우 그 활동의 시작과 종료 시간 앞뒤로 부분 집합이 생긴다. 

따라서 모든 활동에 대해 그 활동이 선택 되고 난 후의 부분 집합들에서 재귀적으로 같은 과정을 반복하면서 최대 값을 찾을 수 있다.

 C[i,j] = max(C[i,k] + C[k,j] + 1)


2. 그리디 알고리즘 방식

최적해에 속할 하나의 활동을 선택하고, 그 다음으로 속할 수 있는 활동을 선택해 나간다. 

즉 현재 상태에서 최적의 해를 하나씩 찾아 나가서 해를 찾을 수 있다.

```
class Activity implements Comparable<Activity>{

    int start;
    int end;

    Activity(int start, int end){
        this.start = start;
        this.end = end;
    }

    @Override
    public int compareTo(Activity activity) {

        if(this.end == activity.end){

            return (this.start - activity.start);
        }
        return this.end - activity.end;
    }

    public String toString(){

        return "{start :" + start + "," + "end :" + end + "}";
    }
}

public class MeetingRoom {

     public static void main(String[] args) {

         TreeSet<Activity> activities = new TreeSet<>();
         Activity activity1 = new Activity(1, 4);
         Activity activity2 = new Activity(3, 5);
         Activity activity3 = new Activity(0, 6);
         Activity activity4 = new Activity(5, 7);
         Activity activity5 = new Activity(3, 8);
         Activity activity6 = new Activity(5, 9);
         Activity activity7 = new Activity(6, 10);
         Activity activity8 = new Activity(8, 11);
         Activity activity9 = new Activity(8, 12);
         Activity activity10 = new Activity(2, 13);
         Activity activity11 = new Activity(12, 14);

         activities.add(activity1);
         activities.add(activity2);
         activities.add(activity3);
         activities.add(activity4);
         activities.add(activity5);
         activities.add(activity6);
         activities.add(activity7);
         activities.add(activity8);
         activities.add(activity9);
         activities.add(activity10);
         activities.add(activity11);


         int count = 0;
         int prev_end_time = 0;

         Iterator<Activity> ir = activities.iterator();
         while(ir.hasNext()){

                Activity activity = ir.next();
                // 직전 종료시간이 다음 회의 시작 시간보다 작거나 같다면 갱신
                if(prev_end_time <= activity.start) {
                    prev_end_time = activity.end;
                    count++;
                    System.out.println(activity);
                }

            }
            System.out.println(count);
        }
}
```

### 3. 그리디 알고리즘으로 해결할 수 있는 문제들

#### 거스름돈 계산하는 문제 

- 동전의 액면이 모두 바로 아래 액면의 배수가 되면 그리디 알고리즘으로 최적해가 보장된다. 

![coin](./img/coin.png)      ![coin2](./img/coin2.png)


- 동전의 액면이 아로 아래 액면의 배수가 되지 않는 경우는 동적 프로그래밍 방식으로 최적해를 구해야 한다.

![coin3](./img/coin3.png)



#### 배낭 채우기 문제

![knapsack](./img/knapsack.png)

- 분할 가능 문제 (그리디로 해결 가능) : 최대 가치의 물건 부터 채워 넣은 후 마지막에 물건을 자를 수 있는 경우

- 0/1 배낭 문제 ( 최적화 문제) : 물건을 자를 수 없는 경우

