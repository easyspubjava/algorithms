# 4.1 이진 트리 (Binary Tree)

### 1. 트리에 대하여

- 용어 살펴보기

    - 부모 (Parent) 노드

    - 자식 (Child) 노드

    - 선조 (Ancestor) 노드

    - 후손 (Descendant) 노드

    - 형제(Sibling) 노드

![tree](./img/tree.png)

- 비선형 자료구조 - 1:1이 아닌 1:N의 관계

- 레벨(Level) : 루트노드로 부터 거리

- 높이(Height) : 단말 노드로부터 최대거리

- 차수 (Degree) : 자식노드의 수


### 2. 이진 트리(Binary Tree)

- 최대 차수(Degree)가 2인 트리, 다양한 알고리즘에 응용되는 자료구조
   
   수행시간이 O(logN) 을 지향하는 자료구조
 

- 포화 이진 트리 

![fullbt](./img/fullybt.png)

   레벨이 h 인 경우 노드의 개수는?

- 완전 이진 트리

![completebt](./img/completebt.png)


- 편향 트리 (skewed tree)

![skewedtree](./img/skewedtree.png)


### 3. 이진 트리 구현하기와 순회하기

![binarytree](./img/binarytree.png)


```
class TreeNode{
	Object data;
	TreeNode left;
	TreeNode right;
}
```

- 순회하기 (traversal)

![traversal](./img/traversaltree.png)

| traversal | order |
| ------ | ------ |
| preorder |   A B D H I E J C F K G L M    |
| inorder |    H D I B J E A F K C L G M   |
| postorder |  H I D J E B K F L M G C A       |

```
public class BinaryTree {

    private TreeNode root;

    public TreeNode makeBinTree(TreeNode rootNode){



        this.root = rootNode;
        rootNode.right = null;
        rootNode.left = null;

        return rootNode;
    }

   public TreeNode insertLeftChildNodeBT(TreeNode parentNode, TreeNode element){

        if(parentNode != null && parentNode.left == null) {

            parentNode.left = element;
            parentNode.left.left = null;
            parentNode.left.right = null;
            return parentNode.left;
        }
        else return null;
   }

    public TreeNode insertRightChildNodeBT(TreeNode parentNode, TreeNode element){

        if(parentNode != null && parentNode.right == null) {

            parentNode.right = element;
            parentNode.right.left = null;
            parentNode.right.right = null;
            return parentNode.right;
        }
        else return null;
    }

    public TreeNode getRootNodeBT()
    {
      return root;
    }


    public TreeNode getLeftChildNodeBT(TreeNode node)
    {
       if (node != null) {
            return node.left;
        }

        return null;
    }

    public TreeNode getRightChildNodeBT(TreeNode node)
    {
        if (node != null) {
            return node.right;
        }

        return null;
    }

    public void preorder(TreeNode root){
        if(root != null){
            System.out.print( root.data);
            preorder(root.left);
            preorder(root.right);
        }
    }
    public void inorder(TreeNode root){
        if(root != null){
            inorder(root.left);
            System.out.print(root.data);
            inorder(root.right);
        }
    }
    public void postorder(TreeNode root){
        if(root != null){
            postorder(root.left);
            postorder(root.right);
            System.out.print(root.data);
        }
    }
}
```

```
public class BinaryTreeTest {


    public static void main(String[] args){

        TreeNode nodeA = new TreeNode();
        TreeNode nodeB = new TreeNode();
        TreeNode nodeC = new TreeNode();
        TreeNode nodeD = new TreeNode();
        TreeNode nodeE = new TreeNode();
        TreeNode nodeF = new TreeNode();
        TreeNode nodeG = new TreeNode();
        TreeNode nodeH = new TreeNode();
        TreeNode nodeI = new TreeNode();
        TreeNode nodeJ = new TreeNode();
        TreeNode nodeK = new TreeNode();
        TreeNode nodeL = new TreeNode();
        TreeNode nodeM = new TreeNode();


        BinaryTree binaryTree = new BinaryTree();

        nodeA.data = "A";
        nodeA = binaryTree.makeBinTree(nodeA);

        if (nodeA != null) {
            nodeB.data = 'B';
            nodeB = binaryTree.insertLeftChildNodeBT(nodeA, nodeB);

            nodeC.data = 'C';
            nodeC = binaryTree.insertRightChildNodeBT(nodeA, nodeC);
        }
        if (nodeB != null) {
            nodeD.data = 'D';
            nodeD = binaryTree.insertLeftChildNodeBT(nodeB, nodeD);
            nodeE.data = 'E';
            nodeE = binaryTree.insertRightChildNodeBT(nodeB, nodeE);
        }
        if (nodeC != null) {
            nodeF.data = 'F';
            nodeF = binaryTree.insertLeftChildNodeBT(nodeC, nodeF);

            nodeG.data = 'G';
            nodeG = binaryTree.insertRightChildNodeBT(nodeC, nodeG);
        }
        if (nodeD != null) {
            nodeH.data = 'H';
            nodeH = binaryTree.insertLeftChildNodeBT(nodeD, nodeH);

            nodeI.data = 'I';
            nodeI = binaryTree.insertRightChildNodeBT(nodeD, nodeI);
        }
        if (nodeE != null) {
            nodeJ.data = 'J';
            nodeJ = binaryTree.insertLeftChildNodeBT(nodeE, nodeJ);
        }
        if (nodeF != null) {
            nodeK.data = 'K';
            nodeK = binaryTree.insertRightChildNodeBT(nodeF, nodeK);
        }
        if (nodeG != null) {
            nodeL.data = 'L';
            nodeL = binaryTree.insertLeftChildNodeBT(nodeG, nodeL);

            nodeM.data = 'M';
            nodeM = binaryTree.insertRightChildNodeBT(nodeG, nodeM);
        }

        System.out.printf("\n Preorder : ");
        binaryTree.preorder(nodeA);

        System.out.printf("\n Inorder : ");
        binaryTree.inorder(nodeA);

        System.out.printf("\n Postorder : ");
        binaryTree.postorder(nodeA);


    }
}
```



