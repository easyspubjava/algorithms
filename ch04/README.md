# 4. 트리와 알고리즘


### [4.1 이진 트리(Binary Tree)](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch04/04-01/README.md)


### [4.2 이진 검색 트리(Binary Search Tree)](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch04/04-02/README.md)


### [4.3 AVL Tree & Red Black Tree](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch04/04-03/README.md)


### [4.4 B-Tree ](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch04/04-04/README.md)


### [4.5 힙(Heap)](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch04/04-05/README.md)
