# 4.3 AVL Tree & Red Black Tree

### 1. AVL Tree란? 

- AVL 트리: 알고리즘 개발자 이름인 Adelson-Velsky and Landis에서 따온 이름

- 균형 인수 BF(Balance Factor): 왼쪽 서브트리의 높이에서 오른쪽 서브트리의 높이를 뺀 값
 
- | HL - HR | ≤ 1


![avl](./img/avl.png)


### 2. AVL 트리의 균형이 깨진경우 balance 맞추기

- Left- Left

![ll](./img/ll.png)

- LL Rotation

![llrotation.png](./img/llrotation.png)


- Right - Right

![rr](./img/rr.png)

- RR Rotation

![rrrotation.png](./img/rrrotation.png)


- Left - Right 

![lr](./img/lr.png)

- LR Rotation

![lrrotation.png](./img/lrrotation.png)


- Right - Left

![rl](./img/rl.png)

- RL Rotation

![rlrotation.png](./img/rlrotation.png)



### 3. Red Black Tree 란?

- 모든 노드는 레드, 블랙 색깔의 가진다. 

- 루트 노드는 블랙이다.

- 모든 리프는 블랙이다. (리프 노드는 마지막 노드가 아닌 NULL을 뜻한다.)

- 레드 노드의 자식노드는 반드시 블랙이다.

- 루트 노드에서 임의의 리프 노드에 이르는 경로에서 만나는 블랙 노드의 수는 모두 같다.

![redblacktree](./img/redblacktree.png)


### 4. Red Black Tree에 데이터 추가하기

- 추가 되는 노드(x)는 레드 노드로 칠한다.

- 노드가 추가되고 나서도 레드 블랙 트리의 조건을 만족해야 한다.


1. 추가 되는 노드(x)의 부모 노드 (p)가 블랙이면 : 아무 조정이 필요없음

2. 추가 되는 노드(x)의 부모 노드 (p)가 레드이면 : 레드 블랙트리의 성질이 깨짐 => 조정이 필요함 (이러한 조정에 의해 균형이 유지됨)

![redblackinsert1](./img/redblackinsert1.png)

2-1.  p가 레드인 경우 p의 부모노드 p2는 반드시 블랙이다. 따라서 이경우 p의 형제 노드(s)의 색은 레드 일수도 있고 블랙 일수도 있다.

![redblackinsert2](./img/reablackinsert2.png)

2-1-1.  s가 레드인 경우

![redblackinsert3](./img/reablackinsert3.png)

2-1-2-1.  s가 블랙인 경우의 오른쪽 자식(x)으로 추가되는 경우

![redblackinsert4](./img/reablackinsert4.png)

2-1-2-2.  s가 블랙이 경우 왼쪽 자식(x)으로 추가되는 경우 

![redblackinsert5](./img/reablackinsert5.png)


### 5. Red Black Tree에 데이터 삭제 하기

- 삭제 노드의 자식이 없거나 1개만 가진 경우로 귀결되는 알고리즘

1. 삭제 노드(m)가 레드이면 아무 문제가 없다.

2. 삭제 노드(m)가 블랙이라도 유일한 자식이 레드이면 문제가 되지 않는다.

![redblackdelete1](./img/redblackdelete1.png)

2-1. 삭제 노드(m)가 블랙이고 유일한 자식이 블랙이면 m 이 삭제후 모든 리프 노드로 가는 블랙 노드의 개수가 -1 이 된다.

![redblackdelete2](./img/redblackdelete2.png)

부모 노드(p)와 주변 노드의 색상에 따라 나뉠 수 있다.

![redblackdelete3](./img/redblackdelete3.png)

2-1-1.  부모 노드(p)가 레드이고 나머지는 모두 블랙일때 

![redblackdelete3-1](./img/reablackdelete3-1.png)

2-1-2,3.  부모 노드(p)가 레드이고 나머지는 s는 블랙인데, 왼쪽, 오른쪽 자식 노드의 색에 따라서

![redblackdelete2-1-2](./img/redblackdelete2-1-2.png)

2-2-1. 부모 노드(p)가 블랙일 때 

![redblackdelete2-2-1](./img/redblackdelete2-2-1.png)


=> 모두 5가지 경우로 구분 

![redblackdelete5](./img/redblackdelete5.png)

=> 삭제하기

![case2delete](./img/case2delete.png)

![case2-1delete](./img/case2-1delete.png)
