# 4.2 이진 검색 트리 (Binary Search Tree)

### 1. 이진 검색 트리란? (BST)

- 검색을 위한 자료구조 

- Key (유일한 값)이 트리의 자료가 됨  : 중복을 허용하지 않음

- 왼쪽 subtree에 있는 모든 키 값은 부모의 키 값보다 작다.

- 오른쪽 subtree에 있는 모든 키 값은 부모의 키 값보다 크다.

- 키 값을 비교하는 기준은 자료의 종류에 따라 다를 수 있음

- 키를 검색할 때 하나의 노드와 비교 후 크거나 작음에 따라 왼쪽 혹은 오른쪽 subtree로의 이동을 결정할 수 있음

- 트리가 비교적 균형을 이루었다면 n 개의 요소에서 어떤 키값을 찾거나 찾는데 실패하는데 걸리는 시간은 평균적으로 log _n_ 이 됨 


![bst](./img/bst.png)

### 2. BST 에서 검색하기

- 14을 찾아봅니다. 

- 15을 찾아봅니다. 

![bst2](./img/bst2.png)

```
private TreeNode searchItem(TreeNode tNode, Comparable searchKey) {
		if (tNode == null) 
			return null;  // 검색 실패
		else if (searchKey.compareTo(tNode.key) == 0)
			return tNode;
		else if (searchKey.compareTo(tNode.key) < 0)
			return searchItem(tNode.left, searchKey);
		else 
			return searchItem(tNode.right, searchKey);
	}
```

### 3. BST에 자료 추가하기

- 자료를 추가하기 위해서는 추가할 위치를 찾아감 (검색이 먼저 이루어짐)

- 만약 이미 자료가 있다면 추가 할 수 없음 (Key는 유일해야 하며 중복을 허용하지 않음)

- 기존에 없는 key 값이라면 즉, 검색에 실패했다면 자료를 추가할 수 있음

```
	public void insert(Comparable newKey) {
		root = insertItem(root, newKey);
	}
 
	private TreeNode insertItem(TreeNode tNode, Comparable newItem) {
		if (tNode == null)  // insert after a leaf  (or into an empty tree)
			tNode = new TreeNode(newItem, null, null);
		else  if (newItem.compareTo(tNode.key) < 0) 	// branch left
			tNode.left = insertItem(tNode.left, newItem);
		else  					// branch right
			tNode.right = insertItem(tNode.right, newItem);
		return tNode; 
	}
```


### 4. BST에서 자료 삭제하기 

- 자식 노드의 개수에 따라 

- case 1: 자식 노드가 없는 경우 

![bstdelete1.png](./img/bstdelete1.png)

- case 2: 자식 노드가 한 개인 경우

![bstdelete2.png](./img/bstdelete2.png)


- case 3: 자식 노드가 두 개인 경우

![bstdelete3.png](./img/bstdelete3.png)

![bstdel2.png](./img/bstdel2.png)

```
	public void delete(Comparable searchKey) {
		root = deleteItem(root, searchKey);
	}
	
	private TreeNode deleteItem(TreeNode tNode, Comparable searchKey) {
		if (tNode == null) return null;	// key not found!
		else {
			if (searchKey == tNode.key) 	// key found at tNode
				tNode = deleteNode(tNode);
			else if (searchKey.compareTo(tNode.key) < 0) 
				tNode.left = deleteItem(tNode.left, searchKey);
			else 
				tNode.right = deleteItem(tNode.right, searchKey);
			return tNode;
		}
	}
```

```
private TreeNode deleteNode(TreeNode tNode) {
	// 3가지 case
		//    1. tNode이 리프 노드
		//    2. tNode이 자식이 하나만 있음
		//    3. tNode이 자식이 둘 있음
		if ((tNode.left == null) && (tNode.right == null))  // case 1(자식이 없음)
			return null;  
		else if (tNode.left == null) 	// case 2(오른자식뿐)
			return tNode.right;
      	else if (tNode.right == null)   	// case 2(왼자식뿐)
       		return tNode.left;	
       else {	 	 	 	 		// case 3(두 자식이 다 있음)
        		returnPair rPair = deleteMinItem(tNode.right);
        		tNode.key = rPair.key; tNode.right = rPair.node;
        		return tNode; 
        	}
	}
```


### 5. 편향 트리(skewed tree)

- binary search tree에서 균형이 깨졌을때의 문제점

![bstree](./img/bstree.png)      ![skewed](./img/skewed.png)

![time](./img/time.png)
