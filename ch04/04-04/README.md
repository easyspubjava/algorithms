# 4.4 B 트리

### 1. 다원 검색 트리

- 균형 이진 검색 트리의 문제점 : 높이가 깊다.

- m원 검색트리의 특징

1. 각 노드는 0개에서 최대 m개의 sub tree를 가진다.

2. k개의 subtree를 가지는 노드는 k-1개의 자료를 가진다.

3. 각 노드 안에서 자료들은 검색 키의 순서로 정렬된다.

4. Key i ≤ (i번째 서브트리 내의 모든 키 값) < Key (i + 1)

![btree](./img/btree.png)


### 2. B Tree 란?

1. 루트 노드는 단말(leaf) 노드이거나, 2에서 m 개의 서브트리를 가진다.

2. 루트 노드를 제외한 모든 내부(internal) 노드는 아래의 개수만큼 서브트리를 가진다.
   ┌ m/2 ┐≤ (서브트리의 개수) ≤ m

3. 단말 노드는 아래의 개수만큼 자료를 가진다.
   ┌ m/2 ┐- 1 ≤ (자료의 개수) ≤ m - 1

4. 모든 단말 노드는 같은 레벨에 있다. 즉, 트리는 완전한 균형 상태에 있도록 한다.

예) ![btreeex](./img/btreeex.png)


### 3. B Tree 에서 검색하기

- 기본적으로 이진 검색 트리와 같음

- 최대 k개의 키를 가진다.

- 노드의 여러개 키중 검색 자료와 일치하는 지를 확인한다. 

- 해당 노드에서 찾으려는 자료가 없는 경우 Key i ≤ 자료 값 < Key (i + 1) 위치를 찾아 분기할 subtree를 찾는다.

- 이진 검새 트리의 재귀 호출과 같은 원리로 구현 가능하다.

### 4. B Tree에 자료 추가하기


![btreeinsert](./img/btreeinsert.png)

![btreeinsert1](./img/btreeinsert1.png)

- 오버플로우와 재분재(redistribution)

![btreeinsert2](./img/btreeinsert2.png)

- 오버플로우와 분할(split)

![btreeinsert3](./img/btreeinsert3.png)

![btreeinsert4](./img/btreeinsert4.png)

- 오버플로우가 발생하여 연속적인 분할 => 터미널의 높이는 유지됨

![btreeinsert5](./img/btreeinsert5.png)



### 5. B Tree에서 자료 삭제하기 


![btreedelete](./img/btreedelete.png)

![btreedelete1](./img/btreedelete1.png)

![btreedelete2](./img/btreedelete2.png)

![btreedelete3](./img/btreedelete3.png)

### 6. B Tree 사용예

![btreepage](./img/btreepage.png)


