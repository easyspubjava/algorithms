# 4.5 힙 (Heap)

### 1. Max Heap

- 부모 노드의 키 값이 자식 노드의 키 값보다 항상 크거나 같다.

- 레벨이 h 인 트리에서 h-1 레벨까지는 포화 이진 트리이고, h 레벨에서는 완전 이진 트리의 구조를 가진다. 

- Max Heap은 루트의 키 값이 트리 모든 다른 노드의 키 값보다 항상 크거나 같다.

- Heap 의 데이터 삭제는 항상 루트에서 삭제되고 삭제후 항상 Max(혹은 Min) Heap의 특성을 유지한다. 

- Min Heap은 Max Heap과 동일한 특성을 가지며, 단 부모의 키 값은 자식 노드의 키 값보다 항상 작거나 같다.

- 자료를 추가하거나 삭제하는데 걸리는 시간 복잡도는 log _n_ 이다.

![maxheap](./img/maxheap.png)


### 2. Heap 구현하기

- 힙은 배열을 활용하여 구현한다. (완전 이진 트리이므로 배열을 활용)

- 노드 i의 부모 노드 인덱스 = └ i/2 ┘, 단, i > 1

- 노드 i의 왼쪽 자식 노드 인덱스 = 2 x i

- 노드 i의 오른쪽 자식 노드 인덱스 = (2 x i) + 1

 => 0 인덱스는 사용하지 않음

### 3. Heap 에 자료 추가하기

```
insertMaxHeap ( heap, data ) { 
    // Step-1) 트리의 마지막 자리에 임시 저장 
    heap->currentElementCount ← heap->currentElementCount + 1 
    i ← heap.currentElementCount 
    // Step-2) 부모 노드와 키 값 비교 및 이동 
    while ((i != 1) && (data.key > heap->pElement[i/2].key) ) { 
        heap->pElement[ i ] ← heap->pElement[i/2] i ← i /2 
    } 
    heap->pElement[ i ] ← data 
}
```

### 4. Heap에서 자료 삭제하기

```
deleteMaxHeap ( heap ) { 
    // Step-1: 루트 노드의 삭제 
    result ← heap->pElement[ 1 ] 
    
    //Step-2) 트리 마지막 자리 노드의 임시 이동 
    i ← heap->currentElementCount 
    temp ← heap->pElement[ i ]
    heap->currentElementCount ← heap->currentElementCount - 1 
    
    //Step-3) 자식 노드와 키 값 비교 및 이동 
    parent ← 1 child ← 2 

    while(child <= heap->currentElementCount) { 
        if ( (child < heap->currentElementCount) 
        && heap->pElement[ child ].key < heap->pElement[ child+1 ].key) { 
            child ← child+1; 
        } 
        if (temp->key >= heap->pElement[ child ].key) { 
            break; 
        } 
        heap->pElement[ parent ] ← heap->pElement[ child ] 
        parent ← child child ← child * 2 
    } 

    heap->pElement[ parent ] ← temp 
    return result 
}

```

```
public class Heap {

	private int HEAP_SIZE;
	private int heapArr[];
	
	public Heap()
	{
		HEAP_SIZE = 0;
		heapArr = new int [50];
	}
	
	public void insertHeap(int input)
	{
		int i = ++HEAP_SIZE;
		while(( i != 1) && ( input > heapArr[i/2])){ //max heap
			heapArr[i] = heapArr[i/2];
			i = i/2;
		}
		heapArr[i] = input;
	}
	
	public int getHeapSize()
	{
		return HEAP_SIZE;
	}
	
	public int deleteHeap()
	{
		int parent, child;
		int data, temp;
		data = heapArr[1];
		
		temp = heapArr[HEAP_SIZE];
		HEAP_SIZE -= 1;
		parent =1; child = 2;
		
		while(child <= HEAP_SIZE){
			if((child < HEAP_SIZE) && (heapArr[child] < heapArr[child+1])){
				child++;
			}
			if(temp >= heapArr[child]) break;
			heapArr[parent] = heapArr[child];
			parent = child;
			child *= 2;
		}
		
		heapArr[parent] = temp;
		return data;
	}
	
	public void printHeap(){
		System.out.printf("\nHeap >>> ");
		for(int i=1; i<=HEAP_SIZE; i++)
			System.out.printf("[%d] ", heapArr[i]);
	}
	public static void main(String[] args) {
		Heap h = new Heap();
		h.insertHeap(13);
		h.insertHeap(8);
		h.insertHeap(10);
		h.insertHeap(15);
		h.insertHeap(20);
		h.insertHeap(19);
		
		h.printHeap();
	
		int n, data;
		n = h.getHeapSize();
		for(int i=1; i<=n; i++){    //N
			data = h.deleteHeap();  //logN
			System.out.printf("\n deleted Item : [%d]", data);
		}
	}
}
```

### 5. Heap의 활용 

- 우선 순위 큐 : 배열에 저장된 데이터 중 가장 높은 혹은 가장 낮은 우선 순위의 자료를 꺼내는데 활용

- Heap 정렬 : 루트에서 삭제되는 데이타는 항상 가장 큰 값이거나 가장 작은 값이므로 n개의 Heap을 활용하면 n개의 데이터를 정렬하는데 걸리는 시간 복잡도는 nlog _n_ 이다.
