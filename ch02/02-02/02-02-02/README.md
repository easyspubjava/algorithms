# 2. 연결 리스트(Linked List)


### 1. 연결 리스트에 대하여

- 동일한 데이터 타입을 순서에 따라 관리하는 자료 구조

- 자료를 저장하는 노드에는 자료와 다음 요소를 가리키는 링크(포인터)가 있음

- 자료가 추가 될때 노드 만큼의 메모리를 할당 받고 이전 노드의 링크로 연결함 (정해진 크기가 없음)

- 자료를 추가하거나 삭제할 때 연결되는 링크의 조정만 하면 됨 : O(1)

- 연결 리스트의 i 번째 요소를 찾는게 걸리는 시간은 요소의 개수에 비례 : O(n)

![node](./img/node.png)

ListNode.java
```
public class ListNode<E> {

	private E data;
	public ListNode<E> next;
	
	public ListNode(){
		data = null;
		next = null;
	}
	
	public ListNode(E data){
		this.data = data;
		this.next = null;
	}
	
	public ListNode(E data, ListNode<E> link){
		this.data = data;
		this.next = link;
	}
	
	public E getData(){
		return data;
	}
}
```

### 2. 연결 리스트에 자료 추가하고 삭제하기

- 연결 리스트에에 자료를 추가하는 경우 

![listinsert](./img/listinsert.png)

- 연결 리스트에서 자료를 삭제하는 경우

![listdelete](./img/listdelete.png)


### 3. 여러 연결 리스트 

- 자료를 효율적으로 관리하기 위해 이중 연결 리스트(double linked list)를 가장 많이 사용함

![listall](./img/listall.png)


###  4. 연결 리스트 구현하기

MyLinkedList.java
```
public class MyLinkedList<E> implements ListInterface<E> {

	private ListNode<E> head;
	int count;
	
	public MyLinkedList()
	{
		head = null;
		count = 0;
	}
	
	public void addElement( E data )
	{
		
		ListNode newNode;
		if(head == null){  
			newNode = new ListNode(data);
			head = newNode;
		}
		else{
			newNode = new ListNode(data);
			ListNode temp = head;
			while(temp.next != null)    
				temp = temp.next;
			temp.next = newNode;
		}
		count++;
	}
	
	public void insertElement(int position, E data )
	{
		int i;
		ListNode<E> tempNode = head;
		ListNode<E> newNode = new ListNode(data);
		
		if(position < 0 || position > count ){
			System.out.println("추가할 위치 오류입니다. 현재 리스트 자료의 개수는 " + count +"개 입니다.");
			return;
		}
		
		if(position == 0){  //first element
			newNode.next = head;
			head = newNode;
		}
		else{
			ListNode<E> preNode = null;
			for(i=0; i<position; i++){
				preNode = tempNode;
				tempNode = tempNode.next;
				
			}
			newNode.next = preNode.next;
			preNode.next = newNode;
		}
		count++;
		return;
	}
	
	public E removeElement(int position)
	{
		int i;
		ListNode<E> tempNode = head;
		
		if(position >= count ){
			System.out.println("삭제할 위치 오류입니다. 현재 리스트 자료의 개수는 \" + count +\"개 입니다.");
			return null;
		}
		
		if(position == 0){  
			head = tempNode.next;
		}
		else{
			ListNode<E> preNode = null;
			for(i=0; i<position; i++){
				preNode = tempNode;
				tempNode = tempNode.next;
			}
			preNode.next = tempNode.next;
		}
		count--;
		System.out.println(position + "번째 항목이 삭제되었습니다.");
		
		return tempNode.getData();
	}
	
	public E getElement(int position)
	{
		int i;
		ListNode<E> tempNode = head;
		
		if(position >= count ){
			System.out.println("검색할 위치 오류입니다. 현재 리스트 자료의 개수는 \" + count +\"개 입니다.");
			return null;
		}
		
		if(position == 0){  

			return head.getData();
		}
		
		for(i=0; i<position; i++){
			tempNode = tempNode.next;
			
		}
		return tempNode.getData();
	}

	public ListNode<E> getNode(int position)
	{
		int i;
		ListNode tempNode = head;
		
		if(position >= count ){
			System.out.println("검색할 위치 오류입니다. 현재 리스트 자료의 개수는 \" + count +\"개 입니다.");
			return null;
		}
		
		if(position == 0){  

			return head;
		}
		
		for(i=0; i<position; i++){
			tempNode = tempNode.next;
			
		}
		return tempNode;
	}

	public void removeAll()
	{
		head = null;
		count = 0;
		
	}
	
	public int getSize()
	{
		return count;
	}
	
	public void printAll()
	{
		if(count == 0){
			System.out.println("출력할 내용이 없습니다.");
			return;
		}
		
		ListNode<E> temp = head;
		while(temp != null){
			System.out.print(temp.getData());
			temp = temp.next;
			if(temp!=null){
				System.out.print("->");
			}
		}
		System.out.println("");
	}
	
	public boolean isEmpty()
	{
		if(head == null) return true;
		else return false;
	}
	
	public void reverseList()
	{
		if(head == null) return;
		
		ListNode<E> currentNode = null;
		ListNode<E> preNode = null;
		ListNode<E> nextNode = head;
		
		while (nextNode != null){
			preNode = currentNode;	
			currentNode = nextNode;	 
			nextNode = nextNode.next;			
			currentNode.next = preNode;
		}
		
		head = currentNode;
		
	}
}
```

MyLinkedListTest.java
```
public class MyLinkedListTest {

	public static void main(String[] args) {

		MyLinkedList list = new MyLinkedList();
		list.addElement("A");
		list.addElement("B");
		list.addElement("C");
		
		//list.removeElement(3);
		list.printAll();
		list.reverseList();
		list.printAll();
		
		
		list.removeElement(0);
		list.removeElement(0);
		list.printAll();

		list.insertElement(0, "A-1");
		list.printAll();
		System.out.println(list.getSize());
		
		list.removeElement(0);
		list.printAll();
		System.out.println(list.getSize());
		
		list.removeAll();
		list.printAll();
		list.addElement("A");
		list.printAll();
		System.out.println(list.getElement(0));
		list.removeElement(0);

	}

}
```
