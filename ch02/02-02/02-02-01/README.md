# 1. 배열 리스트


### 1. 배열 리스트에 대하여

- 동일한 데이터 타입을 순서에 따라 관리하는 자료 구조

- 정해진 크기가 있음(fixed-length)

- 물리적으로 연속된 메모리를 사용하므로 자료의 추가와 제거시 다른 요소들의 이동이 필요함 O(n)

- 물리적으로 연속된 메모리를 사용하므로 배열의 i 번째 자료를 찾는 연산이 빠름 : **인덱스 연산** O(1)


### 2. 배열 리스트에 자료 추가하고 삭제하기

- 배열에 자료를 추가하는 경우 

![arrayinsert](./img/arrayinsert.png)

- 배열에서 자료를 삭제하는 경우

![arraydelete](./img/arraydelete.png)


### 3. 배열 리스트 구현하기

MyArrayList.java
```
public class MyArrayList<E> implements ListInterface<E> {

	private int count;  		//개수
	private E[] objectList;   	//int array
	
	private int DEFAULT_CAPACITY = 10;
	public static final int ERROR_NUM = -999999999;
	
	public MyArrayList()
	{
		objectList = (E[])new Object[DEFAULT_CAPACITY];
	}
	
	public MyArrayList(int size)
	{
		DEFAULT_CAPACITY = size;
		objectList = (E[])new Object[DEFAULT_CAPACITY];
	}
	
	public void addElement(E data)
	{
		if(count >= DEFAULT_CAPACITY){
			System.out.println("not enough memory");
			return;
		}
		objectList[count++] = data;
				
	}
	public void insertElement(int position, E data)
	{
		int i;
		
		if(count >= DEFAULT_CAPACITY){  //
			System.out.println("not enough memory");
			return;
		}
		
		if(position < 0 || position > count ){  //index error
			System.out.println("insert Error");
			return;
		}
		
		if(position == count ){

			objectList[count++] = data;
			return;
		}
		
		for( i = count-1; i >= position ; i--){
			objectList[i+1]  = objectList[i];
		}

		objectList[position] = data;
		count++;
	
	}
	
	public E removeElement(int position)
	{

		if( isEmpty() ){
			System.out.println("There is no element");
			return null;
		}
		
		if(position < 0 || position >= count ){  //index error
			System.out.println("position Error");
			return null;
		}
		
		E ret = objectList[position];
		
		for(int i = position; i<count -1; i++ )
		{
			objectList[i] = objectList[i+1];
		}
		count--;
		return ret;
	}
	
	public int getSize()
	{
		return count;
	}
	
	public boolean isEmpty()
	{
		if(count == 0){
			return true;
		}
		else return false;
	}
	
	public E getElement(int position)
	{
		if(position < 0 || position >= count){
			System.out.println("current count is " + count );
			return null;
		}
		return objectList[position];
	}
	
	public void printAll()
	{
		if(count == 0){
			System.out.println("Array is empty");
			return;
		}
			
		for(int i=0; i<count; i++){
			System.out.println(objectList[i]);
		}
		
	}
	
	public void removeAll()
	{
		objectList = (E[]) new Object[DEFAULT_CAPACITY];
		count = 0;
	}

	public int getCapacity()
	{
		return DEFAULT_CAPACITY;
	}
}
```

MyArrayListTest.java
```
public class MyArrayListTest {

	public static void main(String[] args) {

		MyArrayList array = new MyArrayList();
		array.addElement(10);
		array.addElement(20);

		array.addElement(30);
		array.insertElement(1, 50);
		array.printAll();
		
		System.out.println("===============");
		array.removeElement(1);
		array.printAll();
		System.out.println("===============");
	
		MyArrayList array2 = new MyArrayList();
		array2.addElement(10);
		array2.printAll();
		System.out.println("===============");
		array2.removeElement(1);
		array2.printAll();
		
		System.out.println(array.getElement(2));
		
	}
}
```


