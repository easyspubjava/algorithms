# 2. 큐(Queue) 구현하고 활용하기


### 1. 큐(Queue)에 대하여

- 맨 앞(front)에서 자료를 꺼내고, 맨 뒤(rear)에 자료를 추가함 (중간의 자료를 꺼낼 수 없음)

- First In First Out ( 선입선출 ) 구조

- 한 줄로 서기

- 먼저 추가된 자료가 먼저 꺼내짐

- 시스템에서 이벤트를 핸들링 하기위해 저장하는 구조


### 2. Queue 에서 사용하는 메서드 

- enqueue() : rear의 위치에 자료를 넣음

![enqueue](./img/enqueue.png)

- dequeue() : front의 위치에서 자료를 꺼냄

![dequeue](./img/dequeue.png)

- peek() : front의 위치에 있는 자료를 반환

![peek](./img/peek.png)

```
public interface QueueInterface<E> {
	public void enqueue(E x);
	public E dequeue();
	public E peek();
	public boolean isEmpty();
	public void dequeueAll();
}

```
### 3. 원형 큐 (Circular Queue)

![queueop](./img/queueop.png)


![Circular](./img/circularqueue.png)


### 4. Circular List 구현하기

```
public class CircularLinkedList<E> implements ListInterface<E> {
	private ListNode<E> rear;
	private int numItems;

	public CircularLinkedList() {		
		numItems = 0;
		rear = new ListNode(-1);
		rear.next = rear;
	}

	public void insertElement(int index, E x) {	
		if (index >= 0 && index <= numItems) {
			ListNode<E> prevNode = getNode(index - 1);
			ListNode<E> newNode = new ListNode(x, prevNode.next);
			prevNode.next = newNode;
			if (index == numItems)
				rear = newNode;
			numItems++;
		}
	}

	public void addElement(E x) {
		ListNode<E> prevNode = rear;		
		ListNode<E> newNode = new ListNode(x, rear.next);
		prevNode.next = newNode;
		rear = newNode;
		numItems++;
	}

	public E removeElement(int index) {
		if (index >= 0 && index <= numItems-1) {
			ListNode<E> prevNode = getNode(index - 1);
			E rItem = prevNode.next.getData();
			prevNode.next = prevNode.next.next;
			if (index == numItems)
				rear = prevNode;
			numItems--;
			return rItem;
		} else return null;
	}
	

	public E getElement(int index) {
		if (index >= 0 && index <= numItems-1) {
			return getNode(index).getData();
		} else return null;	// 에러
	}


	private ListNode<E> getNode(int index) {
		if (index >= -1 && index <= numItems) {
			ListNode<E> currNode = rear.next;  // 더미 헤드
			for (int i = 0; i <= index; i++) {
				currNode = currNode.next;
			}
			return currNode; 
		} else {return null;}
	}

	public final int NOT_FOUND = -12345;
	public int indexOf(E x) {		// 원소 x의 인덱스 리턴
		ListNode<E> currNode = rear.next;	// 더미 헤드
		for (int i = 0; i <= numItems-1; i++) {
			currNode = currNode.next;
			if (((Comparable)(currNode.getData())).compareTo(x) == 0) return i;
		}
		return NOT_FOUND; 
	}

	public int getSize() {
		return numItems;
	}

	public boolean isEmpty() { 
		return numItems == 0;
	}

	public void removeAll() {
		numItems = 0;
		rear = new ListNode(-1);
		rear.next = rear;
	 }

	 ///////////////////////////////////////////////////////////////////////
	 public void printAll() {
		ListNode<E> head = rear.next; // 더미 헤드
		System.out.print("Print list (#items=" + numItems + ") ");
		for(ListNode<E> t=head.next; t != head; t = t.next)
			System.out.print(t.getData() + " ");
		System.out.println();
	}
}
```

### 5. Circular List로 Queue구현하기

```
public class MyLinkedQueue<E> extends CircularLinkedList<E> implements QueueInterface<E>{

	private final E ERROR = null; //

	public void enQueue(E data) {
		addElement(data);
	}


	public E deQueue( ) {
		return removeElement(0);
	}


	public E front() {
		if (isEmpty()){
			System.out.println("queue is empty");
			return null;
		}
		else
			return getNode(0).getData();	// rear.next: front
	}


	public boolean isEmpty() {
		return super.isEmpty();
	}


	public void dequeueAll ( ) {
		removeAll();
	}

	/////////////////////////////////////////////////////
	public void printAll() {
		super.printAll();
	}

}

```

### 4. 큐는 어디에 활용될까요?

- **순서에 기반한 처리를 해야 하는 경우**

- **시스템의 이벤트나 메세지를 처리할 때 System Queue를 사용**

- **홈쇼핑이나 금융회사의 전화 상담의 call queue**

- **그래프의 BFS 탐색에서 Queue 활용**
