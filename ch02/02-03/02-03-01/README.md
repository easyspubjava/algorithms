# 1. 스택(Statck) 구현하고 활용하기


### 1. 스택(Stack)에 대하여

- 맨 마지막 위치(top)에서만 자료를 추가,삭제, 꺼내올 수 있음 ( 중간의 자료를 꺼낼 수 없음)

- Last In First Out ( 후입선출 ) 구조

- 택배 상자가 쌓여있는 모양

- 가장 최근의 자료를 찾아오거나 게임에서 히스토리를 유지하고 이를 무를때 사용할 수 있음

- 함수의 메모리는 호출 순서에 따른 stack 구조


### 2. Stack에서 사용하는 메서드 

- push() : top의 위치에 자료를 넣음

![push](./img/push.png)

- pop() : top의 위치에서 자료를 꺼냄

![pop](./img/pop.png)

- peek() : top의 위치에 있는 자료를 반환

![peek](./img/peek.png)

```
public interface StackInterface<E> {
	public void push(E newItem);
	public E pop();
	public E peek();
	public boolean isEmpty();
	public void popAll();
} 
```

### 3. 스택 구현하기

```
public class MyArrayStack<E> extends MyArrayList<E> implements StackInterface<E> {

	int top;
	MyArrayList<E> arrayStack;
	
	public MyArrayStack()
	{
		top = 0;
		arrayStack = new MyArrayList();
	}
	
	public MyArrayStack(int size)
	{
		arrayStack = new MyArrayList(size);
	}
	
	public void push(E data)
	{
		if(isFull()){
			System.out.println("stack is full");
			return;
		}
		
		arrayStack.addElement(data);
		top++;
	}
	
	public E pop()
	{
		if (top == 0){
			System.out.println("stack is empty");
			return null;
		}
		return arrayStack.removeElement(--top);
		
	}
	
	public E peek()
	{
		if (top == 0){
			System.out.println("stack is empty");
			return null;
		}
		return arrayStack.getElement(top-1);
	}
	
	public int getSize()
	{
		return top;
	}
	
	public boolean isFull()
	{
		if(top == arrayStack.getCapacity()){
			return true;
		}
		else return false;
	}
	
	public boolean isEmpty()
	{
		if (top == 0){
			return true;
		}
		else return false;
	}

	@Override
	public void popAll() {
		arrayStack.printAll();
	}

}
```

```
public class MyArrayStackTest {

	public static void main(String[] args) {

		MyArrayStack stack = new MyArrayStack(3);
		
		stack.push(10);
		stack.push(20);
		stack.push(30);
		stack.push(40);
		
		stack.printAll();
		
		System.out.println("top element is " + stack.pop());
		stack.printAll();
		System.out.println("stack size is " + stack.getSize());
	}

}
```

### 4. 스택은 어디에 활용될까요?

- **함수 호출시 사용되는 지역변수가 사용하는 메모리는 스택 구조**

- **쇼핑몰에서 가장 최근에 검색한 항목 유지**

- **체스나 바둑에서 되돌리기 기능**

- **그래프의 DFS 탐색에서 Stack 자료구조 활용**

