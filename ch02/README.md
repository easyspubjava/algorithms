# 2. 리스트와 알고리즘


### [2.1 가장 기본적인 자료구조 리스트](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch02/02-01/README.md)


### [2.2 배열 리스트와 연결 리스트](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch02/02-02/README.md)


### [2.3 리스트로 구현한 스택과 큐](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch02/02-03/README.md)
