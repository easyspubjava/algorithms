 # 5. 정렬 
 
 
### [5.1 O(n*n) 으로 풀이되는 정렬 알고리즘](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch05/05-01/README.md)
### [5.2 O(nlogn) 으로 풀이되는 정렬 알고리즘](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch05/05-02/README.md)
### [5.3 그 외 정렬 알고리즘](https://gitlab.com/easyspubjava/algorithms/-/blob/main/ch05/05-03/README.md)
