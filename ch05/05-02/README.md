# 5.2 O(nlogn) 으로 풀이되는 정렬 알고리즘

### 1. Quick Sort

- 중심 값(피못 :Pivot) 을 기준으로 두 자료의 키 값을 비교하여 위치를 교환

    Left : 피봇보다 큰 자료 찾음 왼쪽->오른쪽,Right까지 이동가능

    Right : 피봇보다 작은 자료 찾음 오른쪽->왼쪽, Left보다 왼쪽으로 이동할 수 있음

	재귀호출

- 추가적인 메모리 사용없이 가장 빠른 정렬 알고리즘 

- 최악의 경우의 시간 복잡도가 좋지 않음

- 평균 시간 복잡도 : O(nlogn)

- 최악 시간 복잡도 : O(n*n)

![quicksort1](./img/quicksort.png)

![quicksort2](./img/quicksort2.png)

![quicksort3](./img/quicksort3.png)

![quicksort4](./img/quicksort4.png)

![quicksort5](./img/quicksort5.png)

```
class QuickSort{
    int i=0;
	public int partition(int value[], int begin, int end){
		int pivot, temp, left, right;

		left = begin;
		right = end;
		pivot = right;
		System.out.printf("\n [QuickSort %d Step : pivot=%d ]\n", ++i, value[pivot]);
		while(left < right){
			while((value[left]<value[pivot]) && (left < right)) left++;
			while((value[right]>=value[pivot]) && (left<right)) right--;
			if(left < right){
				temp = value[left];
				value[left] = value[right];
				value[right] = temp;
			}
		}
		temp = value[pivot];
		value[pivot] = value[right];
		value[right] = temp;
		for(int j=0; j<value.length; j++)
			System.out.printf("%3d  ", value[j]);
		System.out.println();
		return right;
	}

	public void quickSort(int a[], int begin, int end){
		if(begin < end){
			int p;
			p = partition(a, begin, end);
			quickSort(a, begin, p-1);
			quickSort(a, p+1, end);
		}
	}
 }


class QuickSortTest{
	public static void main(String args[]){
		int a[] = {80, 50, 70, 10, 60, 20, 40, 30};
		QuickSort S = new QuickSort();
		System.out.printf("\nTo Sort Elements : \" ");
		for(int i=0; i<a.length; i++)
			System.out.printf(" %d", a[i]);
		System.out.println();
		S.quickSort(a, 0, 7);
	}
}
```

### 2. Merge Sort

- 같은 개수의 원소를 가지는 부분 집합으로 기존 자료를 나누고 

  나눈 자료를 다시 부분 병합을 하며 정렬한다. 

  추가적인 메모리가 필요
  
  재귀호출 

  평균, 최악, 최선 O(nlogn)



![merge](./img/merge.png)

![merge2](./img/merge2.png)



```
class MergeSort{
    private int sorted[] = new int [30];

    public void merge(int a[], int start, int middle, int end){

        int  i, j, k, t;
        i = start;
        j = middle+1;
        k = start;
        while(i <= middle && j <= end){
            if(a[i] <= a[j])  sorted[k] = a[i++];
            else  sorted[k] = a[j++];
            k++;
        }
        if(i > middle){
            for(t=j; t<=end; t++, k++)
                sorted[k] = a[t];
        }
        else{
            for(t=i; t<=middle; t++, k++)
                sorted[k] = a[t];
        }

        for(t=start; t<=end; t++)
            a[t] = sorted[t];
        System.out.printf("\n Merge Sort >> ");
        for(t=0; t<a.length; t++)
            System.out.printf( a[t] + ",");
    }

    public void mergeSort(int a[], int start, int end)	{
        int middle;
        if(start < end){
            middle = (start + end)/2;
            mergeSort(a, start, middle);
            mergeSort(a, middle+1, end);
            merge(a, start, middle, end);
        }
    }
}

public class MergeSortTest {
    public static void main(String args[]) {
        int a[] = {80, 50, 70, 10, 60, 20, 40, 30};
        int size = a.length;
        MergeSort S = new MergeSort();
       
        System.out.println();
        S.mergeSort(a, 0, size-1);
    }
}
```

### 3. Heap Sort

- Heap 자료구조를 이용하여 정렬함
  
  ![heap1](./img/heap1.png)

  ![heap2](./img/heap2.png)

  ![heap3](./img/heap3.png)

  
  평균, 최악, 최선 O(nlogn)
