# 5.1 O(n*n) 으로 풀이되는 정렬 알고리즘

### 정렬이란?

- 자료를 정해진 순서대로 재배열 하는 것

- 오름 차순 / 내림 차순

- 시간 복잡도 n*n, nlog(n)

### 1. Selection Sort

- 정열되지 않은 자료 중에서 해당 위치에 맞는 자료를 선택하여 위치를 교환함

![selection](./img/selection.png)


![selection2](./img/selection2.png)

```
class SelectionSort {
	
	static void swap(int[] a, int idx1, int idx2) {
		int t = a[idx1]; a[idx1] = a[idx2]; a[idx2] = t;
	}

	
	static void selectionSort(int[] a, int n) {
		for (int i = 0; i < n - 1; i++) {
			int min = i;			
			for (int j = i + 1; j < n; j++)
				if (a[j] < a[min])
					min = j;
			swap(a, i, min);		
		}
	}

	public static void main(String[] args) {

		int[] arr = {80, 50, 70, 10, 60, 20, 40, 30};

		selectionSort(arr, 8);			

		System.out.println("오름차순으로...");
		for (int i = 0; i < arr.length; i++)
			System.out.println("x[" + i + "]＝" + arr[i]);
	}
}
```


### 2. Bubble Sort

![bubble](./img/bubble.png)


![bubble2](./img/bubble2.png)


```
class BubbleSort {
	
	static void swap(int[] a, int idx1, int idx2) {
		int t = a[idx1]; 
		a[idx1] = a[idx2]; 
		a[idx2] = t;
	}

	
	static void bubbleSort(int[] a, int n) {
		for (int i = 0; i < n-1 ; i++) {
			for (int j = 0; j < n - i - 1; j++) {
				if (a[j] > a[j + 1])
					swap(a, j, j + 1);
			}
		}
	}

	public static void main(String[] args) {
		int[] arr = {80, 50, 70, 10, 60, 20, 40, 30};

		bubbleSort(arr, arr.length);				

		System.out.println("오름차순으로...");
		for (int i = 0; i < arr.length; i++)
			System.out.println("x[" + i + "]＝" + arr[i]);
	}
}
```


### 3. Insertion Sort

![insertion](./img/insertion.png)


![insertion2](./img/insertion2.png)

```
class InsertionSort {

	static void insertionSort(int[] a, int n) {
		for (int i = 1; i < n; i++) {
			int j;
			int tmp = a[i];
			for (j = i; j > 0 && a[j - 1] > tmp; j--)
				a[j] = a[j - 1];
			a[j] = tmp;
		}
	}

	public static void main(String[] args) {
		int[] arr = {80, 50, 70, 10, 60, 20, 40, 30};

		insertionSort(arr, arr.length);				

		System.out.println("오름차순으로...");
		for (int i = 0; i < arr.length; i++)
			System.out.println("x[" + i + "]＝" + arr[i]);
	}
}
```
