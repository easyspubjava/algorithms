# 5.3 그 외 정렬 알고리즘

### 1. Radix Sort 

- Radix(기수) : 숫자의 자릿수

- 버킷에 자료를 보관했다가 다시 꺼내면서 정렬을 함

- 최선, 평균, 최악 O(d*n)

![radix](./img/radix.png)

![radix2](./img/radix2.png)

![radix3](./img/radix3.png)

