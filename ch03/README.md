# 3. 재귀 호출(Recursion)


### 1. 재귀 호출의 개념

- 분할 정복(Divide and Conquer) : 문제의 타입은 같고 문제의 범위를 작게 하여 해결하는 방법


![recursion](./img/recursion.png)

- 알고리즘의 특성

    - 입력(input)

    - 출력(output)

    - 명백성(definiteness)

    - 유한성(finiteness)

    - 유효성(effectiveness)

- 재귀 호출에서는 종료 조건(Terminate Condition)이 명확해야한다.

### 2. 재귀 호출의 예

- 팩토리얼 계산하기

![factorial](./img/factorial.png)

![factorial2](./img/factorial2.png)


```
public class Factorial {

    public int factorialSum(int num){
        int total;
        if(num == 1).  //종료 조건
            return 1;
        total = num * factorialSum(num-1);
        return total;
    }

    public static void main(String[] args){

        Factorial factorial = new Factorial();
        int total = factorial.factorialSum(10);
        System.out.println(total);
    }
}
```
- 재귀 호출시 스택 메모리가 계속 쌓이고, 각 변수와 반환값등 함수에 대한 환경 값들이 변하는 

Context Switch 가 발생하는데 많은 비용이 소모된다. 종료 조건이 명확하지 않은 경우 stack overflow가 발생함

![overflow](./img/overflow.png)


### 3. 재귀 호출과 반복 호출

![fibonacci](./img/fibonacci.png)


![fibonacci2](./img/fibonacci2.png)

- 피보나치 수열 

```
 public int fibonacciRecur(int n){
        if(n == 0) return 0;
        if(n == 1) return 1;

        return fibonacciRecur(n-1) + fibonacciRecur(n-2);
    }
```

```
public int fibonacciIter(int n){

        int ppre = 0;
        int pre = 1;
        int current = 0;

        if(n == 0) return 0;
        if(n == 1) return 1;

        for(int i=2; i<=n; i++){

            current = ppre + pre;
            ppre = pre;
            pre = current;
        }

        return current;
    }
```

- 선형 수행시간에 문제를 해결할 수 있다.


### 4. 재귀 호출과 동적 프로그래밍

- 동적 프로그래밍은 큰 문제의 답이 작은 문제의 답에 포함되어 있고 이를 재귀 호출로 구현하는 경우 너무 많은 중복이 발생하는 경우에 동적 프로그래밍을 활용하여 해결할 수 있다.

- 부분의 결과를 저장했다가 사용하여 해결할 수 있는 경우 

- 피보나치 수열을 동적 프로그래밍 방식으로 구현하면 선형 시간에 문제를 해결할 수 있다. 

```
   public int fibonacciMem(int n) {

        int[] value = new int[n+1];

        value[0] = 0;
        value[1] = 1;

        if (n == 0) {
            return value[0];
        }

        if (n == 1) {
            return value[1];
        }

        int i;
        for( i = 2; i<=n; i++) {

            value[i] = value[i-1] + value[i-2];

        }
        return value[i-1];
    }
```

