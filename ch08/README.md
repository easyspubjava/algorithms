# 8. 동적프로그래밍

### 1. 동적 프로그래밍이란?

- 부분 문제를 해결하고 테이블에 저장함으로써 다시 필요할때 그 해를 참조하여 문제를 해결하는 프로그래밍 방법

- 큰 문제의 해답에 그보다 작은 문제의 해답을 포함하고 있는 최적해의 구조를 가짐(Optimal Structure)

- 재귀호출을 통하여 구할 수 있음 (피보나치 수열 예)

- 동적 프로그래밍을 적용하는 경우

    1. 최적해의 구조를 가진다.

    2. 재귀호출로 구현하면 비용이 많이 증가하는 경우


### 2. 막대 자르기 문제

    강철 막대의 자르는 곳을 결정하는 문제

    강철 회사는 기다란 강철 맨대를 사서 더 작게 나누어 판매한다. (강철 막대를 자르는 비용은 없다. 여러번 잘라도 비용은 같다는 가정)
    이익이 최대치가 되도록 막대를 잘라보자

    i인치의 가격에 대해 Pi의 가격을 청구한다고 할때 아래의 표와 같다
    길이가 n인치인 막대가 있는 경우 해당 막대를 잘라서 판매하여 얻을 수 있는 수익을 계산해보자
     
![longstick](./img/longstick.png)
(n=4 일때의 예)

### - 동적 프로그래밍으로 해결하는 이유

- 길이가 n이 막대는 
![longstick2](./img/longstick2.png) 개의 다른 방법으로 막대를 자를 수 있다.
    
- 각 인치(n-1)마다 자르거나 자르지 않거나 (0 또는 1)의 독립적인 선택을 할 수 있다.

- 만약 길이가 7인 막대를 7 = 2 + 2 + 3 이라고 썼다면 각각 2, 2, 3의 길이 3개로 분할 하였다는 의미

- 길이가 n인 막대가 k개로 분해되어 최적해가 되었을 때의 ( 1<= k <= n) 조각의 식은 ![longstick3](./img/longstick3.png)

- 이때의 최대 수익은 ![longstick4](./img/longstick4.png)

- 즉, n>=1 에 대한 최대 수익의 값은 더 적은 막대로 부터 최대 수익을 이용해 얻을 수 있다. 

- 두 조각으로 나눌 수 있는 최대 수익은 나누지 않을 때와 두 조각으로 나눈 모든 경우에서 최대의 이익이 된다.
    
![longstick5](./img/longstick5.png)

    - 크기가 n인 원래의 문제를 해결하기 위해 더 작은 크기의 문제를 푼다.Optimal Structure를 가진 문제임

![longstick6](./img/longstick6.png)

    - 이전 해에서 가졌던 가장 큰 값을 계속 참조하며 문제를 해결

### - 메모하기(memoization)을 활용한 하향식으로 구현하기

    - 재귀적으로 쓰지만, 배열이나 해시테이블에 저장하여 그 값을 다시 활용하여 해결

![longstick8](./img/longstick8.png)

![longstick7](./img/longstick11.png)

![longstick9](./img/longstick12.png)

### - 상향식으로 구현하기

![longstick10](./img/longstick10.png)


```
public class CuttingRod {

        
       
        public static int rodCut(int[] price, int n)
        {

            int[] T = new int[n + 1];
            int maxCount = 0;
            
            for (int i = 1; i <= n; i++)
            {
                // 길이가 `i`인 막대를 길이가 `j`인 막대 두 개로 나눕니다.
                // `i-j`는 각각 최대값을 취합니다.
                for (int j = 1; j <= i; j++) {
                   //T[i] = Integer.max(T[i], price[j - 1] + T[i - j]);
                    if( T[i] < price[j - 1] + T[i - j]){
                        T[i] = price[j - 1] + T[i - j];
                        maxCount = j;

                    }

                }
            }

            System.out.println(maxCount);
            return T[n];
        }

        public static void main(String[] args)
        {
            int[] price = { 1, 5, 8, 9, 10, 17, 17, 20 };
            int n = 4;        // 로드 길이

            System.out.print("Profit is " + rodCut(price, n));
        }
}
```

   
### 3. 행렬 경로 문제

![matrix2](./img/matrix1.png)

![matrix2](./img/matrix2.png)

![matrix3](./img/matrix3.png)

![matrix4](./img/matrix4.png)

![matrix5](./img/matrix5.png)


```
public class MatrixPath {

    public static void main(String[] args) {

        int[][] matrix = {{6, 7, 12, 5},
                {5, 3, 11, 18},
                {7, 17, 3, 3},
                {8, 10, 14, 9}
        };

        int[][] pathCost = new int[4][4];
        pathCost[0][0] = matrix[0][0];

        for(int i=0; i<4; i++){
            for(int j=0; j<4; j++){
                cost(matrix, i, j, pathCost);
            }
        }

        System.out.println(pathCost[3][3]);
    }

    public static int cost(int[][] matrix, int i, int j, int[][] pathCost){

        if(i == 0 && j == 0) return pathCost[0][0];

        if(i == 0) return pathCost[i][j] = pathCost[0][j-1] + matrix[0][j];
        if(j == 0) return pathCost[i][j] = pathCost[i-1][0] + matrix[i][0];

        return pathCost[i][j] = Math.max(pathCost[i-1][j], pathCost[i][j-1]) + matrix[i][j];
    }
}
```





