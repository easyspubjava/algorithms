# 6.3 그래프와 알고리즘들

### 1. MST ( Minumum Spanning Tree ) 알고리즘

#### Spanning Tree 란?

![mst](./img/mst.png)

#### Minimum Spanning Tree

- 모든 노드가 연결되는 Spanning Tree중 가장 가중치가 작은 Tree

- MST 알고리즘은 최적의 해를 보장하는 흔하지 않은 Greedy Algorithms

#### Kruskal Algorithms

- 알고리즘 개요

  1) 그래프에서 모든 간선을 오름차순으로 정렬한다.

  2) 간선 중에 가중치가 제일 적으면서 순환(cycle)을 발생하지 않는 간선을 추가한다.
     
     간선이 추가 될때마다 V(G)에 노드도 추가한다.

  3) 그래프의 모든 노드 V(G)에 포함될 때까지( 모든 노드가 연결될 때까지) 2)의 과정을 반복한다.


```
class Edge implements Comparable<Edge>{
    int to;
    int from;
    public int value;

    public Edge(int to, int from, int value) {
        this.to = to;
        this.from = from;
        this.value = value;
    }

    @Override
    public int compareTo(Edge o) {
        return this.value - o.value;
    }
}

public class KruskalMST {

    private static int n;
    private static int[] parents;

    public static void main(String[] args) {
        n = 7;
        int[][] graph = {{1, 0, 4},
                {1, 2, 2},
                {0, 2, 3},
                {2, 3, 1},
                {3, 4, 1},
                {3, 5, 5},
                {5, 4, 6},
        };

        parents = new int[n];
        for (int i = 0; i < n ; i++) {
            parents[i] = i;
        }

        Queue<Edge> pq = new PriorityQueue<>();
        for (int i = 0; i < graph.length; i++) {
            int to = graph[i][0];
            int from = graph[i][1];
            int value = graph[i][2];

            pq.add(new Edge(to, from, value));
        }

        int size = pq.size();
        int total = 0;

        for (int i = 0; i < size; i++) {
            Edge edge = pq.poll();
            int rx = find(edge.to);
            int ry = find(edge.from);

            // 사이클이 발생하지 않는 경우에만 집합에 포함
            if (!isSameParent(rx, ry)) {
                total += edge.value;
                union(edge.to, edge.from);
            }
        }
        System.out.println(total);
    }

    static int find(int x) {
        if (parents[x] == x) {
            return x;
        }
        return parents[x] = find(parents[x]);
    }

    static void union(int x, int y) {
        x = find(x);
        y = find(y);
        // 더 find 값으로 부모 노드 설정
        if (x < y) {
            parents[y] = x;
        } else {
            parents[x] = y;
        }
    }

    static boolean isSameParent(int x, int y) {
        if (x == y) return true;
        return false;
    }
}
```


#### Prim Algorithms

- 알고리즘 개요

  1) V(T)에 하나의 노드를 포함시키고
  
  2) V(T)와 연결된 가선중 가장 가중치가 작으면서 순환을 발생시키지 않는 Edge를 선택

  3) 이 Edge와 연결된 노드를 V(T)에 추가 E(T)에는 간선 추가

  4) 모든 노드가 V(T)에 포함될 때 까지 2) 3)의 과정을 반복


```
class Vertex implements Comparable<Vertex>{
    int to;
    int value;

    public Vertex(int to, int value) {
        this.to = to;
        this.value = value;
    }

    @Override
    public int compareTo(Vertex o) {
        return this.value - o.value;
    }
}

public class PrimMST {
    static int total;
    static List<Vertex>[] list;

    static boolean[] visited;
    public static void main(String[] args){
        int v = 6;
        int[][] graph = {{1, 0, 4},
                {1, 2, 2},
                {0, 2, 3},
                {2, 3, 1},
                {3, 4, 1},
                {3, 5, 5},
                {5, 4, 6},
        };


        list = new ArrayList[v];
        visited = new boolean[v];
        for(int i=0; i<v; i++) {
            list[i] = new ArrayList<>();
        }

        for(int i=0; i<graph.length; i++) {
            int from = graph[i][0];
            int to = graph[i][1];
            int weight = graph[i][2];

            list[from].add(new Vertex(to,weight));
            list[to].add(new Vertex(from,weight));
        }

        prim(0);
        System.out.println(total);
    }

    static void prim(int start) {
        Queue<Vertex> pq = new PriorityQueue<>();
        pq.add(new Vertex(start,0));

        while(!pq.isEmpty()) {
            Vertex fromVertex = pq.poll();
            int toVertex = fromVertex.to;
            int weight = fromVertex.value;

            if(visited[toVertex]) continue;
            // 선택한 간선의 정점으로부터 가장 낮은 가중치 갖는 정점 선택 
            visited[toVertex]= true;
            total += weight;

            for(Vertex next : list[toVertex]) {
                if(!visited[next.to]) {
                    pq.add(next);
                }
            }
        }
    }
}
```


### 2. 최단거리 알고리즘

#### Dijkstra Algorithms

  - 단일 시작점에서 다른 노드로 가는 최단 거리를 구하는 문제

   ![dijkstra](./img/dijkstra.png)

  - 시작노드에서 v까지의 최단 거리를 안다고 할때,  시작 노드에서 w로 가는 거리가 v를 거쳐감으로써 더 작아지면 거쳐가는 값으로 변경한다.

   ![dijkstra2](./img/dijkstra2.png)

  
  - 알고리즘 개요

   1) 모든 노드의 집합 S, 시작 노드 r, 다른 노드들 w

   2) 집합 S중에서 최단 거리를 가진 노드 v를 찾아 제거

   3) 노드 v에 인접한 노드 w에 대해 다음 조건이 만족하면 거리를 업데이트
   
       ![dijk](./img/dijk.png)
    
   4) 집합 S의 모든 노드가 제거 될때까지 2) 3)을 반복


  - 음의 가중치를 허용하지 않는다.
 
     : 임의의 점점을 집합 S에 포함할 때 시작점에서 그 정점까지의 최단 거리는 이미 계산 되었다는 가정이 있으므로 음의 가중치가 있게 되면 오류가 발생한다.



  ![dijk2](./img/dijk2.png)

```
class MyGraph{
    private int count;   //노드 수           
    private int[][] vertexMatrix;  // matrix로 그래프 표시
    private int[] distance;        // 특정 노드에 대한 각 노드의 최단 거리
    private boolean[] visited;     // already visited???
    private static int UNLIMIT = 999999999;  // 초기값 
    
    public MyGraph(int count){
        this.count = count;
        vertexMatrix = new int[count][count];
        distance = new int[count];
        visited = new boolean[count];
         
    }
    public void addEdges(int from, int to, int weight){
    	vertexMatrix[from][to] = weight;
    	vertexMatrix[to][from] = weight;
    }
 
    public void calcShotestPath(int from){
         
        for(int i=0;i<count;i++){
            distance[i] = UNLIMIT;
        }
        
        visited[from] = true;
        distance[from] = 0;
        //연결노드 distance갱신
        for(int i= 0; i<count; i++){
            if(visited[from] && vertexMatrix[from][i] !=0){
                distance[i] = vertexMatrix[from][i];
            }
        }
         
        for(int k =0; k<count-1; k++){

            int min=UNLIMIT;
            int minIndex= -1;
            
            for(int i = 0; i< count ;i++){
                if(!visited[i] && distance[i]!=UNLIMIT){
                    if(distance[i] < min ){
                        min = distance[i];
                        minIndex = i;
                    }
                }
            }
            
            visited[minIndex] = true;
            for(int i=0; i<count; i++){
                if(!visited[i] && vertexMatrix[minIndex][i]!=0){
                    if(distance[i]>distance[minIndex]+vertexMatrix[minIndex][i]){
                        distance[i] = distance[minIndex]+vertexMatrix[minIndex][i];
                    }
                }
            }
 
        }
    }
    
    public void showDistance(int from) {
    	
    	for(int i = 0; i<count; i++) {
    		
    		System.out.println(from + " 노드로부터 " + i + " 노드의 최단 거리는 : " + distance[i]);
    	}
    }
}


public class ShortestPath {

	public static void main(String[] args) {
		MyGraph graph = new MyGraph(6);
		graph.addEdges(0, 1, 1);
		graph.addEdges(0, 2, 4);
		graph.addEdges(1, 2, 2);
		graph.addEdges(2, 3, 1);
		graph.addEdges(3, 4, 8);
		graph.addEdges(3, 5, 3);
		graph.addEdges(4, 5, 4);

		graph.calcShotestPath(0);
		graph.showDistance(0);

	}
}
```

####  Floyd-Warshall Algorithms

- 모든 정점 들간의 상호 최단 거리 구하기

- 알고리즘 개요

![floyd](./img/floyd.png)


![floyd2](./img/floyd2.png)

![floyd3](./img/floyd3_mid.png)
  

### 3. 위상 정렬 알고리즘 (Topological Sorting Algorithms)

- 사이클이 없는 단방향성 그래프에서 

- 모든 정점을 일렬로 나열한다.

- 정점 v 에서 w로 가는 간선이 있으면 v는 w보다 항상 앞에 위치한다.

- 일반적으로 임의의 단방향성 그래프에서는 복수의 위상 순서가 존재 할 수 있다. 


- 위상 정렬의 예

![topology](./img/topology.png)

![topology2](./img/topology2.png)

- 알고리즘 개요

  1) 진입 간선(indgree)이 없는 정점 v를 선택한다. 

  2) v를 순서 리스트 VList 에 포함한다.

  3) 정점 v와 v에서 진출 간선(outdegree)의 간선들을 모두 제거한다. 

  4) 모든 정점이 VList에 포함될때까지 1) - 3)을 반복한다. 

  5) VList에는 위상 정렬된 정점들이 순서대로 있다.


```
class Node{

    String action;
    boolean visited;
    List<Node> inNeighbors;
    List<Node> outNeighbors;

    Node(String action){

        this.action = action;
        this.inNeighbors = new ArrayList<>();
        this.outNeighbors = new ArrayList<>();
    }

    public void addNeighbors(Node neighborNode){
        this.outNeighbors.add(neighborNode);
        neighborNode.inNeighbors.add(this);
    }

    public List<Node> getInNeighbors(){return inNeighbors;}
    public List<Node> getOutNeighbors(){return outNeighbors;}

    public String toString(){

        return "" + action;

    }

}
public class TopologicalSort {

    public static void main(String arg[]) {
        TopologicalSort topological = new TopologicalSort();
        Node nodeA = new Node("냄비에 물붓기");
        Node nodeB = new Node("라면 봉지 뜯기");
        Node nodeC = new Node("점화");
        Node nodeD = new Node("라면 넣기");
        Node nodeE = new Node("스프 넣기");
        Node nodeF = new Node("계란 풀어 넣기");

        ArrayList<Node> nodeList = new ArrayList<>();

        nodeList.add(nodeA);
        nodeList.add(nodeB);
        nodeList.add(nodeC);
        nodeList.add(nodeD);
        nodeList.add(nodeE);
        nodeList.add(nodeF);

        nodeA.addNeighbors(nodeC);
        nodeB.addNeighbors(nodeD);
        nodeB.addNeighbors(nodeE);
        nodeC.addNeighbors(nodeD);
        nodeC.addNeighbors(nodeE);
        nodeC.addNeighbors(nodeF);
        nodeD.addNeighbors(nodeF);
        nodeE.addNeighbors(nodeF);

        ArrayList<Node> resultList = new ArrayList<>();

        for(int i=0; i<nodeList.size(); i++){
            Node node = nodeList.get(i);

            if( node.inNeighbors.size() == 0 && !node.visited){

                node.visited = true;
                resultList.add(node);

                for(int j=0; j<node.outNeighbors.size(); j++){
                    Node toNode = node.outNeighbors.get(j);
                    toNode.inNeighbors.remove(node);
                }
            }
        }

        Iterator ir = resultList.iterator();
        while(ir.hasNext()){
            System.out.println(ir.next());
        }

    }

}
```
