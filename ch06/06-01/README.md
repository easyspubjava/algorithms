# 6.1  그래프 구현하기

### 1. 그래프 용어들

- Vertex

- Edge

- Degree

- Adjacent

- Weight

- Path

- Directed Graph / Undirected Graph

- Complete graph / Subgraph

- G = (V, E)

![graph](./img/graph.png)

![complete](./img/completegraph.png)


### 2. 그래프 구현하기

1. 인접 행렬 (Adjacency Matrix)을 이용한 구현

![admatrix](./img/admatrix.png)

![admatrix2](./img/admatrix2.png)

n개의 vertex가 사용된다고 할때 n * n의 공간이 필요하고 O(n * n)의 소요시간이 필요하다. 만약 간선의 밀집도가 적은 경우에는 오버헤드가 큰 방법이다.  

2. 인접 리스트(Adjacency List)를 이용한 구현

![adlist](./img/adlist.png)

