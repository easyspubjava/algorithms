# 7.2 해싱 (Hashing)

### 1. 해싱이란?

- 검색 키(Key)의 산술 연산에 의해 자료가 저장된 위치를 찾는 검색 방법

- 가장 빠른 검색 O(1)

![hashing](./img/hashing.png)

- **해시 테이블(Hash Table)** : 

    - 원소가 저장될 자리가 원소의 (Key)값에 의해 정해지는 자료구조

    - 자료를 저장, 검색, 삭제 하는데 상수시간이 소요

    - 해싱 함수에 의해 저장될 위치가 정해지는데 같은 위치가 반환되는 collosion(충돌)이 발생할 수 있음   

    - 해시테이블에서 충돌이 발생하는 경우

     ![hashtable](./img/hashtable.png)

    - 충돌이 발생하지 않는 이상적인 해시 테이블은 많은 메모리의 낭비

- **해싱 함수(Hashing function)** 

    -  낮은 충돌 발생 빈도 

    -  높은 해시 테이블의 사용률 

    -  빠른 계산    

    -  나머지/곱하기 연산등

### 2. 충돌 (collosion) 

- **clustering** : 해시 함수에 의해 자료들이 해시 테미블에 골고루 분포하지 않고 특정 주소 주위로 뭉치는 경우 
    
- 체이닝

![chainning](./img/chainning.png)
     

- 개방주소 (open addressing)

    - 선형 조사법 (Linear Probing) : 일정한 상수를 증가 시켜 다시 조사, 1차 clustering이 발생할 가능성이 큼

            h(k) = (k + try_count) mod mod
        
        ![linear](./img/linear.png)

    
    - 제곱 조사법 (Quadratic Probing ) : 조사 횟수의 제곱만큼 증가 

            h(k) = (k + try_count * try_count) mod mod

        ![quadratic](./img/quadratic.png)

    - 더블 해싱(Double Hashing) : 원래의 해시 함수와 다른 또다른 함수를 이용

            h(k) = (k + 조사 간격) mod mod
            (조사 간격) = M - (k mod M)
        
        ![double](./img/double.png)


### 4. 적재 밀도(load factor)

- 해시 테이블이 전체에서 얼마나 자료를 저장하고 있는지의 비율

- 높은 적재률은 잦은 충돌이 생기고, 낮은 적재률을 메모리의 낭비 

- 해시테이블의 적재률은 검색의 효츌성과 높은 관련성을 가짐

- 자바 JDK는 0.75 이상이 되면 해시 테이블을 늘림

