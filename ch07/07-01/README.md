# 7.1 검색이란?

- 저장된 자료중에서 원하는 자료를 찾는 것

- key를 기반으로 자료를 찾음

### 1 자료구조에 따른 검색의 복잡도

- 이전 자료구조들을 활용해 검색을 할 경우 시간복잡도가 적지 않음

1. 순차 검색 : 정렬된 자료에 대한 검색 주로 배열 O(n)

2. 색인 순차 검색 : 정렬된 데이터로 부터 색인 테이블을 만들어 색인 테이블을 먼저 검색하고 자료를 검색할 수 있어 검색을 빨라짐 O(m + n/m) ( 검색대상이 되는 전체 자료 n개, 인덱스 테이블의 인덱스 개수 m개)

3. 이진 검색 : 정렬된 데이타 기반의 검색 O(logN)

4. 검색 트리 : 이진 검색 트리, AVL Tree, Red-Black Tree, B Tree  O(logN)

5. 해싱 : 키를 계산하여 저장된 위치를 찾음 O(1) 가장 빠른 검색

### 2. 색인 순차 검색

- 색인(Index) : 특정 키 값을 가지는 자료의 위치

- 색인 테이블(Index Table) : 인덱스를 모아둔 테이블, 검색 범위 축소

![indextable](./img/indextable.png)


![indextable2](./img/indextable2.png)


- 색인 순차 검색에서 검색의 범위

 
![indexsearch](./img/indexsearch.png)

- 수행시간 

    O( m + n/m)

    m : 인텍스 테이블의 인덱스 개수

    n: 검색 대상이 되는 자료의 개수

인덱스 테이블과 자료의 개수에 비례한다. 이때 인덱스 테이블에 몇개의 인덱스를 둘 것이가가 중요한 부분이고

인덱스 테이블의 인덱스가 많아지면 2차, 3차 인덱스 테이블을 만들 수도 있다.


### 3. 이진 검색

- 검생의 범위를 절반으로 감소시켜 가면서 검색

- 정렬이 되어있는 자료를 기반으로 수행

- 수행 시간: O(logN)

![binarysearch](./img/binarysearch.png)


```
public class BinarySearchProblem {

	public static void main(String[] args) {

		int[] numbers = {12, 25, 31, 48, 54, 66, 70, 83, 95, 108};
		
		int target = 83;
		 
		int left = 0;
		int right = numbers.length-1;
		int mid = (left + right)/2;
		
		int temp = numbers[mid];
		boolean find = false;
		
		while(left <= right) {
			
			if(target == temp) {  //수를 찾은 경우
				find = true;
				break;
			}
			else if(target < temp) { // 찾으려는 수가 더 작은 경우
				right = mid-1;
				
			}
			else {
				left = mid+1;
			}
			mid = (left + right)/2;
			temp = numbers[mid];
		}
		
		if(find == true) 
		{
			mid++;
			System.out.println("찾는 수는 " + mid + "번째 있습니다.");
		}
		else System.out.println("찾는 수가 없습니다.");
	}
}

```

